process.env.DB_DATABASE = process.env.DB_DATABASE || "studenthome_testdb"
process.env.NODE_ENV = "testing"
process.env.LOGLEVEL = "debug"

const chai = require("chai")
const chaiHttp = require("chai-http")
const server = require("../../server")
const database = require("../../src/config/database")

chai.should()
chai.use(chaiHttp)

const CLEAR_DB = "DELETE IGNORE FROM `user`"

describe("Authentication", () => {
    before((done) => {
        database.query(CLEAR_DB, (err, rows, fields) => {
            if (err) {
                done(err)
            } else {
                done()
            }
        })
    })

    describe("UC101 Registration", () => {
        it("TC-101-1 should throw error 400 when no firstname is provided", (done) => {
            chai
                .request(server)
                .post("/api/register")
                .send({
                    lastname: "LastName",
                    email: "test@test.nl",
                    studentnr: 1234567,
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-101-2 should throw error 400 when an invalid email is provided", (done) => {
            chai
                .request(server)
                .post("/api/register")
                .send({
                    firstname: "FirstName",
                    lastname: "LastName",
                    email: "test.nl",
                    studentnr: 1234567,
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-101-3 should throw error 400 when no password is given", (done) => {
            chai
                .request(server)
                .post("/api/register")
                .send({
                    firstname: "FirstName",
                    lastname: "LastName",
                    email: "test.nl",
                    studentnr: 1234567,
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-101-5 should return a token when providing valid information", (done) => {
            chai.request(server)
                .post("/api/register")
                .send({
                    firstname: "FirstName",
                    lastname: "LastName",
                    email: "test@test.nl",
                    studentnr: 1234567,
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
        it("TC-101-4 should throw error 400 if the user already exists", (done) => {
            chai.request(server)
                .post("/api/register")
                .send({
                    firstname: "FirstName",
                    lastname: "LastName",
                    email: "test@test.nl",
                    studentnr: 1234567,
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
    })

    describe("UC102 Login", () => {
        it("TC-102-1 should throw error 400 if a required field is missing", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-2 should throw error 400 when email is invalid", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "testtest.nl",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-3 should throw error 400 when no password is given", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "test@test.nl"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-4 should throw error 400 if the user does not exist", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "test12345@test.nl",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(400)
                    done()
                })
        })
        it("TC-102-5 should return a token when providing valid information", (done) => {
            chai
                .request(server)
                .post("/api/login")
                .send({
                    email: "test@test.nl",
                    password: "secret"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    done()
                })
        })
    })
})
