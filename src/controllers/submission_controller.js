const logger = require("tracer").console()
const assert = require("assert")
const database = require("../config/database")

module.exports = {
    create: (req, res, next) => {
        logger.log("submission-controller.create called")

        const userId = req.userId
        const homeId = req.params.homeId
        const mealId = req.params.mealId
        logger.log(`VARIABLES:
        userId: ${userId}
        homeId: ${homeId}
        mealId: ${mealId}`)

        const query = {
            sql: "INSERT INTO participants(UserID, StudenthomeID, MealID, SignedUpOn) VALUES(?, ?, ?, NOW())",
            values: [userId, homeId, mealId]
        }
        const checkMaxParticipants = {
            sql: `SELECT meal.MaxParticipants
                  FROM meal
                  WHERE meal.ID = ?;`,
            values: mealId
        }
        const checkCurrentSubscribers = {
            sql: `SELECT *
                  FROM participants
                  WHERE participants.MealID = ?;`,
            values: mealId
        }
        database.query(checkMaxParticipants, (err, result) => {
            if (err) {
                logger.log("Error checking max participants")
                next({
                    message: "error checking max participants",
                    errCode: 500
                })
            } else {
                const maxParticipants = result
                logger.log(maxParticipants)
                database.query(checkCurrentSubscribers, (err2, result2) => {
                    if (err2) {
                        logger.log("Error checking current subscribers")
                        next({
                            message: "error checking current subscribers",
                            errCode: 500
                        })
                    } else {
                        if (result2 > maxParticipants) {
                            next({
                                message: "This meal is at max capacity",
                                errCode: 400
                            })
                        } else {
                            database.query(query, (err3, result3) => {
                                if (err3) {
                                    logger.log("Error adding submission")
                                    let message
                                    let errCode = 400
                                    if (err3.message.match("ER_DUP_ENTRY")) {
                                        logger.log(err3.message)
                                        message = "A submission with the given data already exists"
                                    } else if (err3.message.match(/*a foreign key constraint fails*/)) {
                                        message = "This meal does not exist"
                                        errCode = 404
                                    } else {
                                        message = err3.message
                                        errCode = 500
                                    }
                                    next({
                                        message: message,
                                        errCode: errCode
                                    })
                                } else if (result3) {
                                    logger.log("Adding submission...")
                                    let submission = {}
                                    submission.id = result3.insertId
                                    submission.userId = userId
                                    submission.homeId = homeId
                                    submission.mealId = mealId
                                    res.status(200).json({
                                        status: "success",
                                        result: submission
                                    })
                                }
                            })
                        }
                    }
                })
            }
        })
    },

    delete: (req, res, next) => {
        logger.log("submission-controller.delete called")

        const userId = req.userId
        const homeId = req.params.homeId
        const mealId = req.params.mealId

        const query = {
            sql: "DELETE FROM participants WHERE UserID = ? AND StudenthomeID = ? AND MealID = ?",
            values: [userId, homeId, mealId],
            timeout: 3000
        }
        let submission = {}
        database.query({
            sql: "SELECT * FROM participants WHERE UserID = ? AND StudenthomeID = ? AND MealID = ?",
            values: [userId, homeId, mealId],
        }, (err, result) => {
            if (result) {
                submission = result
            }
        })
        database.query(query, (err, result) => {
            if (err) {
                logger.log("Error deleting submission")
                next({
                    message: err.message,
                    errCode: 400
                })
            } else if (result.affectedRows == 0) {
                logger.log("Submission does not exist")
                next({
                    message: "Submission does not exist",
                    errCode: 404
                })
            } else if (result) {
                res.status(200).json({
                    status: "success",
                    submission
                })
            }
        })
    },
    
    getListOfParticipants: (req, res, next) => {
        logger.log("submission_controller.getListOfParticipants called")
        
        const userId = req.userId
        const mealId = req.params.mealId
        
        const query = {
            sql: `SELECT *
                  FROM participants
                  WHERE MealID = ?;`,
            values: mealId
        }
        const checkIfMealAvailable = {
            sql: `SELECT *
                  FROM meal
                  WHERE ID = ? AND UserID = ?;`,
            values: [mealId, userId]
        }
        database.query(checkIfMealAvailable, (err, result) => {
            logger.log("checking if meal with parameters exists")
            if (err) {
                logger.log("Error while getting meal")
                next({
                    message: "error while getting meal",
                    errCode: 500
                })
            } else if (result == "") {
                logger.log("This meal could not be found")
                next({
                    message: "This meal does not exist, or you are not it's administrator",
                    errCode: 404
                })
            } else {
                logger.log("getting list of participants")
                database.query(query, (err2, result2) => {
                    if (err2) {
                        logger.log("Error while getting list of participants")
                        next({
                            message: "Error while getting list of participants",
                            errCode: 500
                        })
                    } else {
                        logger.debug(result2)
                        res.status(200).json({
                            status: "success",
                            result: result2
                        })
                    }
                })
            }
        })
    },
    
    getDetailsAboutParticipant: (req, res, next) => {
        logger.log("submission_controller.getDetailsAboutParticipant called")
        
        const userId = req.userId
        const mealId = req.params.mealId
        const participantId = req.params.participantId
        
        const query = {
            sql: `SELECT U.First_Name, U.Last_Name, U.Email, U.Student_Number
                  FROM user AS U
                  JOIN participants AS P ON P.UserID = U.ID
                  WHERE P.MealID = ? AND U.ID = ?;`,
            values: [mealId, participantId]
        }
        const isAdmin = {
            sql: `SELECT M.UserID
                  FROM meal AS M
                  WHERE M.ID = ?;`,
            values: mealId
        }
        database.query(isAdmin, (err, result) => {
            logger.log("Checking if user is admin of this meal")
            if (err) {
                logger.log("Error while getting the records")
                next({
                    message: "error while checking if you are admin",
                    errCode: 500
                })
            } else if (result == "") {
                logger.log("This meal does not exist")
                next({
                    message: "A meal with this id does not exist",
                    errCode: 400
                })
            } else {
                let adminId = result[0].UserID
                if (userId !== adminId) {
                    next({
                        message: "you are not authorized to view participant details",
                        errCode: 403
                    })
                } else {
                    logger.log("getting participant details")
                    database.query(query, (err2, result2) => {
                        if (err2) {
                            logger.log("Error while getting participant details")
                            next({
                                message: "error while getting participant details",
                                errCode: 500
                            })
                        } else if (result2 == "") {
                            next({
                                message: "this user has not subscribed to this meal",
                                errCode: 404
                            })
                        } else {
                            logger.debug(result2)
                            res.status(200).json({
                                status: "success",
                                result: result2
                            })
                        }
                    })
                }
            }
        })
    }
}
