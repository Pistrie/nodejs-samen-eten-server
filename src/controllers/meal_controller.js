const logger = require("tracer").console()
const assert = require("assert")
const database = require("../config/database")

module.exports = {
    validateMeal(req, res, next) {
        logger.log("Validate meal")
        try {
            const {name, description, ingredients, allergies, price, maxParticipants} = req.body
            assert(typeof name === "string", "name is missing!")
            assert(typeof description === "string", "description is missing!")
            assert(typeof ingredients === "string", "ingredients are missing!")
            assert(typeof allergies === "string", "allergies are missing!")
            assert(typeof price === "string", "price is missing!")
            assert(typeof maxParticipants === "number", "max participants is missing!")

            logger.log("Meal data is valid")
            next()
        } catch (err) {
            logger.log("Meal data is invalid:", err.message)
            next({
                message: err.message,
                errCode: 400
            })
        }
    },

    create: (req, res, next) => {
        logger.log("meal-controller.create called")

        const homeId = req.params.homeId

        const {name, description, ingredients, allergies, price, maxParticipants} = req.body

        const query = {
            sql: `INSERT INTO meal(Name, Description, Ingredients, Allergies, CreatedOn, OfferedOn, Price, UserID, StudenthomeID, MaxParticipants)
                    VALUES (?, ?, ?, ?, NOW(), NOW(), ?, ?, ?, ?)`,
            values: [name, description, ingredients, allergies, price, req.userId, homeId, maxParticipants],
            timeout: 3000
        }
        database.query(query, (err, result) => {
            if (err) {
                logger.log("Error adding meal")
                next({
                    message: err.message,
                    errCode: 400
                })
            } else if (result) {
                let meal = {}
                meal.id = result.insertId
                meal.name = name
                meal.description = description
                meal.ingredients = ingredients
                meal.allergies = allergies
                meal.price = price
                meal.userId = req.userId
                meal.studenthomeId = homeId
                meal.maxParticipants = maxParticipants
                res.status(200).json({
                    status: "success",
                    result: meal
                })
            }
        })
    },

    update: (req, res, next) => {
        logger.log("meal-controller.update called")

        const homeId = req.params.homeId
        const mealId = req.params.mealId
        const {name, description, ingredients, allergies, price, studenthomeId, maxParticipants} = req.body

        const query = {
            sql: `UPDATE meal SET Name=?, Description=?, Ingredients=?, Allergies=?, Price=?, MaxParticipants=?
                  WHERE ID = ? AND StudentHomeID = ? AND UserId = ?`,
            values: [name, description, ingredients, allergies, price, maxParticipants, mealId, homeId, req.userId],
            timeout: 3000
        }
        let mealFromStartingQuery = {}
        database.query({
            sql: "SELECT * FROM meal WHERE ID = ?",
            values: mealId
        }, (err, result) => {
            if (result) {
                if (result == "") {
                    next({
                        message: "meal not found",
                        errCode: 404
                    })
                } else {
                    Object.keys(result).forEach(function(key) {
                        mealFromStartingQuery = result[key]
                    })
                    if (mealFromStartingQuery.UserID !== req.userId) {
                        next({
                            message: "you are not the administrator of this meal",
                            errCode: 401
                        })
                    } else {
                        database.query(query, (err, result) => {
                            if (err) {
                                logger.log("Error updating meal", mealId)
                                next({
                                    message: err.message,
                                    errCode: 400
                                })
                            } else if (result.affectedRows == 0) {
                                logger.log("Meal with id", mealId, "does not exist, or the user is not authorized")
                                next({
                                    message: "Meal does not exist, or you are not authorized to change this meal",
                                    errCode: 401
                                })
                            } else if (result) {
                                let meal = {}
                                meal.name = name
                                meal.description = description
                                meal.ingredients = ingredients
                                meal.allergies = allergies
                                meal.price = price
                                meal.userId = req.userId
                                meal.studenthomeId = studenthomeId
                                meal.maxParticipants = maxParticipants
                                res.status(200).json({
                                    status: "success",
                                    result: meal
                                })
                            }
                        })
                    }
                }
            }
        })
    },

    getMealsByHomeId: (req, res, next) => {
        logger.log("meal-controller.getMealsByHomeId called")

        const homeId = req.params.homeId

        const query = {
            sql: "SELECT * FROM meal WHERE StudenthomeID = ?",
            values: [homeId],
            timeout: 3000
        }
        database.query(query, (err, result) => {
            if (result == 0) {
                logger.log("Student home does not exist")
                next({
                    message: "Student home does not exist",
                    errCode: 404
                })
            } else if (err) {
                logger.log("Error getting student home by id")
                next({
                    message: err.message,
                    errCode: 400
                })
            } else if (result) {
                res.status(200).json({
                    status: "success",
                    result: result
                })
            }
        })
    },

    getMealDetailsById: (req, res, next) => {
        logger.log("meal-controller.getMealDetailsById called")

        const homeId = req.params.homeId
        const mealId = req.params.mealId

        const query = {
            sql: "SELECT * FROM meal WHERE StudenthomeID = ? AND ID = ?",
            values: [homeId, mealId],
            timeout: 3000
        }
        database.query(query, (err, result) => {
            console.log(result)
            if (result == 0) {
                logger.log("Meal not found")
                next({
                    message: "Meal does not exist",
                    errCode: 404
                })
            } else if (err) {
                logger.log("Error getting meal")
                next({
                    message: err.message,
                    errCode: 400
                })
            } else if (result) {
                res.status(200).json({
                    status: "success",
                    result: result
                })
            }
        })
    },

    delete: (req, res, next) => {
        logger.log("meal-controller.delete called")

        const homeId = req.params.homeId
        const mealId = req.params.mealId

        const query = {
            sql: "DELETE FROM meal WHERE StudenthomeID = ? AND ID = ?",
            values: [homeId, mealId],
            timeout: 3000
        }
        let meal = {}
        database.query({
            sql: "SELECT * FROM meal WHERE StudenthomeId = ? AND ID = ?",
            values: [homeId, mealId]
        }, (err, result) => {
            if (err) {
                next({
                    message: "an error occured while deleting the meal",
                    errCode: 500
                })
            } else if (result) {
                // everything goes inside the if because it is all
                // dependent on the result of the query
                logger.log(result)
                if (result == "") {
                    next({
                        message: "Meal not found",
                        errCode: 404
                    })
                } else {
                    Object.keys(result).forEach(function(key) {
                        meal = result[key]
                    })
                    logger.log(`
                    meal.UserID: ${meal.UserID}
                    req.userId: ${req.userId}`)
                    if (meal.UserID !== req.userId) {
                        next({
                            message: "You are not this meal's administrator",
                            errCode: 401
                        })
                    } else {
                        database.query(query, (err, result) => {
                            if (err) {
                                logger.log("Error deleting meal from student home")
                                next({
                                    message: err.message,
                                    errCode: 400
                                })
                            } else if (result.affectedRows == 0) {
                                logger.log("Meal with id", mealId, "in student home with id", homeId, "does not exist")
                                next({
                                    message: "Meal in given home does not exist",
                                    errCode: 400
                                })
                            } else if (result) {
                                res.status(200).json({
                                    status: "success",
                                    meal
                                })
                            }
                        })
                    }
                }
            } else {
                next({
                    message: "Something went wrong",
                    errCode: 500
                })
            }
        })
    }
}
