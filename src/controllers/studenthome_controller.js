const logger = require("tracer").console()
const assert = require("assert")
const database = require("../config/database")
const studenthome_dao = require("../dao/studenthomeDao")

module.exports = {
    validateStudentHome(req, res, next) {
        logger.log("Validate student home")
        logger.log("userId:", req.userId)
        logger.log(req.body)
        try {
            const {name, street, number, postalCode, phoneNumber, city} = req.body
            assert(typeof name === "string", "name is missing!")
            assert(typeof street === "string", "street is missing!")
            assert(typeof number === "number", "home number is missing!")
            assert(typeof phoneNumber === "string", "phone number is missing!")
            assert(typeof postalCode === "string", "postal code is missing!")
            assert(typeof city === "string", "city is missing!")
            assert.match(phoneNumber, /^[0-9]{10}$/, "invalid phone number!")
            assert.match(postalCode, /^[1-9][0-9]{3}[A-Z]{2}$/, "postal code doesn't look like regex [1-9][0-9]{3}[A-Z]{2}")

            logger.log("Home data is valid")
            next()
        } catch (err) {
            logger.log("Home data is invalid:", err.message)
            next({
                message: err.message,
                errCode: 400
            })
        }
    },
    
    validateAddUserToHome(req, res, next) {
        logger.log("Validating user to be added to home")
        logger.log("userId:", req.userId)
        logger.log("homeId:", req.params.homeId)
        logger.log(req.body)
        try {
            const userToBeAdded = req.body.userId
            assert(typeof userToBeAdded === "number", "new user is missing!")
            logger.log("data is valid")
            next()
        } catch (err) {
            logger.log("Error:", err.message)
            next({
                message: err.message,
                errCode: 400
            })
        }
    },

    create: (req, res, next) => {

        // format
        // studenthome_dao.create( values, (err, res) => {
        //   if (err) {
        //     afhandelen error
        //   }
        //   res.status

        logger.log("studenthome_controller.create called")

        const { name, street, number, postalCode, phoneNumber, city } = req.body
        const userId = req.userId
        const values = {
            "name": name,
            "street": street,
            "number": number,
            "userId": userId,
            "postalCode": postalCode,
            "phoneNumber": phoneNumber,
            "city": city
        }

        studenthome_dao.create(values, (err, result) => {
            if (err) {
                next({
                    message: err,
                    errCode: 400
                })
            } else {
                res.status(200).json({
                    status: "success",
                    result: result
                })
            }
        })
    },

    getByNameAndCity: (req, res, next) => {
        logger.log("studenthome-controller.getByNameAndCity called")

        let name = req.query.name
        let city = req.query.city
        if (name == undefined || name == "") {
            name = "%"
        }
        if (city == undefined || city == "") {
            city = ""
        }

        let sql
        let values
        if (name == "%" && city == "") {
            logger.log("No values given")
            sql = "SELECT * FROM studenthome"
            values = []
        } else if (city.length < 1) {
            logger.log("Only given name")
            sql = "SELECT * FROM studenthome WHERE Name LIKE ?"
            values = ["%" + name + "%"]
        } else {
            logger.log("Gave both values or only gave city")
            sql = "SELECT * FROM studenthome WHERE Name LIKE ? AND City = ?"
            values = ["%" + name + "%", city]
        }

        const query = {
            sql: sql,
            values: values,
            timeout: 3000
        }
        database.query(query, (err, result) => {
            if (err) {
                logger.log("Error getting home by name and city")
                next({
                    message: err.message,
                    errCode: 400
                })
            } else if (result) {
                if (result == 0 && (city != "" || name != "")) {
                    next({
                        message: "No homes found with your parameters",
                        errCode: 404
                    })
                } else {
                    res.status(200).json({
                        status: "success",
                        result: result
                    })
                }
            }
        })
    },

    getById: (req, res, next) => {
        logger.log("studenthome-controller.getById called")

        const studenthomeId = req.params.homeId

        const query = {
            sql: "SELECT * FROM studenthome WHERE ID = ?",
            values: [studenthomeId],
            timeout: 3000
        }
        database.query(query, (err, result) => {
            if (result == 0) {
                logger.log("Student home does not exist")
                next({
                    message: "Student home does not exist",
                    errCode: 404
                })
            } else if (err) {
                logger.log("Error getting student home by id")
                next({
                    message: err.message,
                    errCode: 400
                })
            } else if (result) {
                res.status(200).json({
                    status: "success",
                    result: result
                })
            }
        })
    },

    update: (req, res, next) => {
        logger.log("studenthome-controller.update called")

        const homeId = req.params.homeId
        const {name, street, number, postalCode, phoneNumber, city} = req.body

        const query = {
            sql: "UPDATE studenthome SET Name=?, Address=?, House_Nr=?, Postal_Code=?, Telephone=?, City=? WHERE ID = ?",
            values: [name, street, number, postalCode, phoneNumber, city, homeId],
            timeout: 3000
        }
        let studenthomeFromStartingQuery = {}
        database.query({
            sql: "SELECT * FROM studenthome WHERE ID = ?",
            values: homeId
        }, (err, result) => {
            if (result) {
                // everything goes inside the if because it is all
                // dependent on the result of the query
                if (result == "") {
                    next({
                        message: "student home not found",
                        errCode: 404
                    })
                } else {
                    Object.keys(result).forEach(function(key) {
                        studenthomeFromStartingQuery = result[key]
                        // console.log(studenthomeFromStartingQuery.UserID)
                    })
                    // console.log(studenthomeFromStartingQuery)
                    if (studenthomeFromStartingQuery.UserID !== req.userId) {
                        next({
                            message: "You are not the administrator of this home",
                            errCode: 403
                        })
                    } else {
                        database.query(query, (err, result) => {
                            if (err) {
                                logger.log("Error updating student home", homeId)
                                next({
                                    message: err.message,
                                    errCode: 400
                                })
                            } else if (result.affectedRows == 0) {
                                logger.log("Student home with id", homeId, "does not exist")
                                next({
                                    message: "Student home does not exist",
                                    errCode: 404
                                })
                            } else if (result) {
                                let studenthome = {}
                                studenthome.name = name
                                studenthome.street = street
                                studenthome.number = number
                                studenthome.userId = req.userId
                                studenthome.postalCode = postalCode
                                studenthome.phoneNumber = phoneNumber
                                studenthome.city = city
                                res.status(200).json({
                                    status: "success",
                                    result: studenthome
                                })
                            }
                        })
                    }
                }
            }
        })
    },

    delete: (req, res, next) => {
        logger.log("studenthome-controller.delete called")

        const homeId = req.params.homeId

        const query = {
            sql: "DELETE FROM studenthome WHERE ID = ?",
            values: [homeId],
            timeout: 3000
        }
        let studenthomeFromStartingQuery = {}
        database.query({
            sql: "SELECT * FROM studenthome WHERE ID = ?",
            values: [homeId],
            timeout: 3000
        }, (err, result) => {
            // everything goes inside the if because it is all
            // dependent on the result of the query
            logger.log(result)
            if (result == "") {
                next({
                    message: "student home not found",
                    errCode: 404
                })
            } else {
                Object.keys(result).forEach(function(key) {
                    studenthomeFromStartingQuery = result[key]
                })
                if (studenthomeFromStartingQuery.UserID !== req.userId) {
                    next({
                        message: "You are not the administrator of this home",
                        errCode: 401
                    })
                } else {
                    database.query(query, (err, result) => {
                        if (err) {
                            logger.log("Error deleting student home", homeId)
                            next({
                                message: err.message,
                                errCode: 400
                            })
                        } else if (result.affectedRows == 0) {
                            logger.log("Student home with id", homeId, "does not exist")
                            next({
                                message: "Student home does not exist",
                                errCode: 404
                            })
                        } else if (result) {
                            res.status(200).json({
                                status: "success",
                                studenthome: studenthomeFromStartingQuery
                            })
                        }
                    })
                }
            }
        })
    },

    addStudentToStudentHome: (req, res, next) => {
        logger.log("studenthome_controller.addStudentToStudentHome called")

        const homeId = req.params.homeId
        logger.debug(`homeId: ${homeId}`)
        const userId = req.body.userId
        
        if (userId == undefined) {
            next({
                message: "no userId provided",
                errCode: 400
            })
        }
        
        const query = {
            sql: `INSERT INTO home_administrators
                    VALUES (?, ?)`,
            values: [userId, homeId]
        }
        let studentHomeFromStartingQuery = {}
        database.query({
            sql: "SELECT * FROM studenthome WHERE ID = ?",
            values: homeId
        }, (err, result) => {
            if (result == "") {
                next({
                    message: "student home not found",
                    errCode: 404
                })
            } else {
                Object.keys(result).forEach(function(key) {
                    studentHomeFromStartingQuery = result[key]
                })
                if (studentHomeFromStartingQuery.UserID !== req.userId) {
                    next({
                        message: "You are not the administrator of this home",
                        errCode: 403
                    })
                } else {
                    database.query(query, (err, result) => {
                        
                        if (err) {
                            logger.log("Error adding user to student home", homeId)
                            let message
                            let errCode
                            if (err.message.match("ER_DUP_ENTRY")) {
                                message = "This user has already been added to this home"
                                errCode = 400
                            } else {
                                message = err.message
                                errCode = 500
                            }
                            next({
                                message: message,
                                errCode: errCode
                            })
                        } else if (result) {
                            res.status(200).json({
                                status: "success",
                                result: "added user " + userId + " to home " + homeId
                            })
                        }
                    })
                }
            }
        })
    }
}
