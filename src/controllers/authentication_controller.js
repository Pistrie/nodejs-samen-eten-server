const assert = require("assert")
const jwt = require("jsonwebtoken")
const database = require("../config/database")
const logger = require("../config/config").logger
const jwtSecretKey = require("../config/config").jwtSecretKey

module.exports = {
    validateLogin(req, res, next) {
        logger.log("validateLogin called")
        // Verify that we receive the expected input
        try {
            assert(typeof req.body.email === "string", "email is missing!")
            assert.match(req.body.email, /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/, "invalid email address")
            assert(typeof req.body.password === "string", "password is missing!")
            logger.log("Userdata is valid")
            next()
        } catch (err) {
            logger.log("Userdata is invalid:", err.message)
            next({
                message: err.message,
                errCode: 400
            })
        }
    },

    validateRegister(req, res, next) {
        logger.log("validateRegister called")
        try {
            assert(typeof req.body.firstname === "string", "first name is missing!")
            assert(typeof req.body.lastname === "string", "last name is missing!")
            assert(typeof req.body.email === "string", "email is missing!")
            assert.match(req.body.email, /[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/, "invalid email address")
            assert(typeof req.body.password === "string", "password is missing!")
            logger.log("userdata is valid")
            next()
        } catch (err) {
            logger.debug("validateRegister error:", err.message)
            next({
                message: err.message,
                errCode: 400
            })
        }
    },

    validateToken(req, res, next) {
        logger.info("validateToken called")
        // The headers should contain the authorization-field with value 'Bearer [token]'
        const authHeader = req.headers.authorization
        if (!authHeader) {
            logger.warn("Authorization header missing!")
            next({
                message: "Authorization header missing!",
                errCode: 401
            })
        } else {
            // Strip the word 'Bearer ' from the headervalue
            const token = authHeader.substring(7, authHeader.length)

            jwt.verify(token, jwtSecretKey, (err, payload) => {
                if (err) {
                    logger.warn("Not authorized")
                    next({
                        message: "Not authorized",
                        errCode: 401
                    })
                } else if (payload) {
                    logger.debug("token is valid", payload)
                    // User heeft toegang. Voeg UserId uit payload toe aan
                    // request, voor ieder volgend endpoint
                    req.userId = payload.id
                    next()
                }
            })
        }
    },

    login(req, res, next) {
        logger.log("authentication-controller.login called")
        const query = {
            sql: "SELECT `ID`, `Email`, `Password`, `First_Name`, `Last_Name` FROM `user` WHERE `Email` = ?",
            values: [req.body.email],
            timeout: 3000
        }
        database.query(query, (err, rows, fields) => {
            // 1. kijken of het useraccount bestaat
            if (err) {
                logger.log("Error:", err.message)
                next({
                    message: err.message,
                    errCode: 500
                })
            } else if (rows) {
                // 2. er was een resultaat, check het password
                logger.info("result from database:")
                logger.info(rows)
                if (rows.length === 1 && rows[0].Password === req.body.password) {
                    logger.info("passwords DID match, sending valid token")
                    // Create an object containing the data we want in the payload
                    const payload = {
                        id: rows[0].ID
                    }
                    // userinfo returned to the caller
                    const userinfo = {
                        id: rows[0].ID,
                        firstName: rows[0].First_Name,
                        lastName: rows[0].Last_Name,
                        emailAddress: rows[0].Email,
                        token: jwt.sign(payload, jwtSecretKey, {expiresIn: "2h"})
                    }
                    logger.debug("Logged in, sending:", userinfo)
                    res.status(200).json({status: "success", result: userinfo})
                } else {
                    logger.info("User not found or password invalid")
                    next({
                        message: "User not found or password invalid",
                        errCode: 400
                    })
                }
            }
        })
    },

    register(req, res, next) {
        logger.log("register called")
        logger.info("register")
        logger.info(req.body)

        const {firstname, lastname, email, studentnr, password} = req.body
        const query = {
            sql: "INSERT INTO `user` (`First_Name`, `Last_Name`, `Email`, `Student_Number`, `Password`) VALUES (?, ?, ?, ?, ?)",
            values: [firstname, lastname, email, studentnr, password],
            timeout: 3000
        }
        database.query(query, (err, rows, fields) => {
            if (err) {
                // if there is an error we assume the user already exists
                logger.error("Error:", err.message)
                let message
                if (err.message.match("ER_DUP_ENTRY")) {
                    message = "This email address is already in use"
                } else {
                    message = err.message
                }
                next({
                    message: message,
                    errCode: 400
                })
            } else if (rows) {
                logger.trace(rows)
                // Create an object containing the data we want in the payload
                // This time we add the id of the newly inserted user
                const payload = {
                    id: rows.insertId
                }
                // Userinfo returned to the caller
                const userinfo = {
                    id: rows.insertId,
                    firstname: firstname,
                    lastname: lastname,
                    emailAddress: email,
                    token: jwt.sign(payload, jwtSecretKey, {expiresIn: "2h"})
                }
                logger.debug("Registered", userinfo)
                res.status(200).json({status: "success", result: userinfo})
            }
        })
    },

    renewToken(req, res, next) {
        logger.debug("renewToken")

        const query = {
            sql: "SELECT * FROM `user` WHERE `ID` = ?",
            values: [req.userId],
            timeout: 3000
        }
        database.query(query, (err, rows, fields) => {
            if (err) {
                logger.error("Error:", err.message)
                next({
                    message: err.message,
                    errCode: 500
                })
            } else if (rows) {
                // User gevonden, return user info met nieuw token.
                // Create an object containing the data we want in the payload
                const payload = {
                    id: rows[0].ID
                }
                // Userinfo returned to the caller
                const userinfo = {
                    id: rows[0].ID,
                    firstName: rows[0].First_Name,
                    lastName: rows[0].Last_Name,
                    emailAddress: rows[0].Email,
                    token: jwt.sign(payload, jwtSecretKey, {expiresIn: "2h"})
                }
                logger.debug("Sending:", userinfo)
                res.status(200).json(userinfo)
            }
        })
    }
}
